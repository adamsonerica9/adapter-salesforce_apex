{
  "id": "@itentialopensource/adapter-salesforce_apex",
  "type": "Adapter",
  "export": "SalesforceApex",
  "title": "Salesforce_apex",
  "src": "adapter.js",
  "roles": [
    "admin"
  ],
  "methods": [
		{
			"name": "iapUpdateAdapterConfiguration",
			"summary": "Updates the adapter configuration",
			"description": "Updates the adapter configuration file with the provided changes",
			"input": [
				{
					"name": "configFile",
					"type": "string",
					"info": "The name of the file to change",
					"required": true,
					"schema": {
						"title": "configFile",
						"type": "string"
					}
				},
				{
					"name": "changes",
					"type": "object",
					"info": "JSON object containing the configuration changes",
					"required": true,
					"schema": {
						"title": "changes",
						"type": "object"
					}
				},
				{
					"name": "entity",
					"type": "string",
					"info": "The entity in which the changes are being made",
					"required": false,
					"schema": {
						"title": "entity",
						"type": "string"
					}
				},
				{
					"name": "type",
					"type": "string",
					"info": "The type of file to change - action, schema, or mock",
					"required": false,
					"schema": {
						"title": "type",
						"type": "string"
					}
				},
				{
					"name": "action",
					"type": "string",
					"info": "The action to be changed",
					"required": false,
					"schema": {
						"title": "action",
						"type": "string"
					}
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapUpdateAdapterConfiguration"
			},
			"task": true
		},
		{
			"name": "iapFindAdapterPath",
			"summary": "Provides the ability to see if a particular API path is supported by the adapter",
			"description": "Provides the ability to see if a particular API path is supported by the adapter",
			"input": [{
				"name": "apiPath",
				"type": "string",
				"info": "The API Path you want to check - make sure to not include base path and version",
				"description": "The API Path you want to check - make sure to not include base path and version",
				"schema": {
					"title": "apiPath",
					"type": "string"
				},
				"required": true
			}],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapFindAdapterPath"
			},
			"task": true
		},
		{
			"name": "iapSuspendAdapter",
			"summary": "Suspends the adapter",
			"description": "Suspends the adapter",
			"input": [{
				"name": "mode",
				"type": "enum",
				"enumerals": ["pause", "error"],
				"info": "How incoming requests are handled. Defaults to 'pause'",
				"description": "How incoming requests are handled. Defaults to 'pause'",
				"schema": {
					"title": "mode",
					"type": "string"
				},
				"required": false
			}],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the adapter suspended status",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapSuspendAdapter"
			},
			"task": true
		},
		{
			"name": "iapUnsuspendAdapter",
			"summary": "Unsuspends the adapter",
			"description": "Unsuspends the adapter",
			"input": [],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the adapter suspended status",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapUnsuspendAdapter"
			},
			"task": true
		},
		{
			"name": "iapGetAdapterQueue",
			"summary": "Return the requests that are waiting in the queue if throttling is enabled",
			"description": "Return the requests that are waiting in the queue if throttling is enabled",
			"input": [],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the adapter queue",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapGetAdapterQueue"
			},
			"task": true
		},
		{
			"name": "iapTroubleshootAdapter",
			"summary": "Runs troubleshoot script for adapter",
			"description": "Runs troubleshoot script for adapter",
			"input": [
				{
					"name": "props",
					"type": "object",
					"info": "Object containing configuration, healthcheck and auth properties {'connProps':{'host': 'api.service.com', 'base_path': '/', 'protocol': 'http', 'port': 443, 'version': 'v1'},'healthCheckEndpoint': '/healthcheck', 'auth': {'auth_method': 'no authentication', 'username': 'username', 'password': 'password'}}",
					"required": true,
					"schema": {
						"title": "props",
						"type": "object"
					}
				},
				{
					"name": "persistFlag",
					"type": "boolean",
					"info": "Whether the input properties should be saved",
					"required": true
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the test results",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapTroubleshootAdapter"
			},
			"task": true
		},
		{
			"name": "iapRunAdapterHealthcheck",
			"summary": "Runs healthcheck script for adapter",
			"description": "Runs healthcheck script for adapter",
			"input": [],
			"output": {
				"name": "result",
				"type": "boolean",
				"description": "Whether healthcheck passed or failed"
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapRunAdapterHealthcheck"
			},
			"task": true
		},
		{
			"name": "iapRunAdapterConnectivity",
			"summary": "Runs connectivity check script for adapter",
			"description": "Runs connectivity check script for adapter",
			"input": [],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the test results",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapRunAdapterConnectivity"
			},
			"task": true
		},
		{
			"name": "iapRunAdapterBasicGet",
			"summary": "Runs basicGet script for adapter",
			"description": "Runs basicGet script for adapter",
			"input": [],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing the test results",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapRunAdapterBasicGet"
			},
			"task": true
		},
		{
			"name": "iapMoveAdapterEntitiesToDB",
			"summary": "Moves entities from an adapter into the IAP database",
			"description": "Moves entities from an adapter into the IAP database",
			"input": [],
			"output": {
				"name": "res",
				"type": "object",
				"description": "A JSON Object containing status, code and the response from the mongo transaction",
				"schema": {
					"title": "res",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapMoveAdapterEntitiesToDB"
			},
			"task": true
		},
		{
			"name": "genericAdapterRequest",
			"summary": "Makes the requested generic call",
			"description": "Makes the requested generic call",
			"input": [
				{
					"name": "uriPath",
					"type": "string",
					"info": "the path of the api call - do not include the host, port, base path or version",
					"description": "the path of the api call",
					"schema": {
						"title": "uriPath",
						"type": "string"
					},
					"required": true
				},
				{
					"name": "restMethod",
					"type": "string",
					"info": "the rest method (GET, POST, PUT, PATCH, DELETE)",
					"description": "the rest method (GET, POST, PUT, PATCH, DELETE)",
					"schema": {
						"title": "restMethod",
						"type": "string"
					},
					"required": true
				},
				{
					"name": "queryData",
					"type": "object",
					"info": "the query parameters to be put on the url (optional)",
					"description": "the query parameters to be put on the url (optional)",
					"schema": {
						"title": "queryData",
						"type": "object"
					},
					"required": false
				},
				{
					"name": "requestBody",
					"type": "object",
					"info": "the payload to be sent with the request (optional)",
					"description": "the payload to be sent with the request (optional)",
					"schema": {
						"title": "requestBody",
						"type": "object"
					},
					"required": false
				},
				{
					"name": "addlHeaders",
					"type": "object",
					"info": "additional headers to be put on the call (optional)",
					"description": "additional headers to be put on the call (optional)",
					"schema": {
						"title": "addlHeaders",
						"type": "object"
					},
					"required": false
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/genericAdapterRequest"
			},
			"task": true
		},
		{
			"name": "genericAdapterRequestNoBasePath",
			"summary": "Makes the requested generic call with no base path or version",
			"description": "Makes the requested generic call with no base path or version",
			"input": [
				{
					"name": "uriPath",
					"type": "string",
					"info": "the path of the api call - do not include the host, port, base path or version",
					"description": "the path of the api call",
					"schema": {
						"title": "uriPath",
						"type": "string"
					},
					"required": true
				},
				{
					"name": "restMethod",
					"type": "string",
					"info": "the rest method (GET, POST, PUT, PATCH, DELETE)",
					"description": "the rest method (GET, POST, PUT, PATCH, DELETE)",
					"schema": {
						"title": "restMethod",
						"type": "string"
					},
					"required": true
				},
				{
					"name": "queryData",
					"type": "object",
					"info": "the query parameters to be put on the url (optional)",
					"description": "the query parameters to be put on the url (optional)",
					"schema": {
						"title": "queryData",
						"type": "object"
					},
					"required": false
				},
				{
					"name": "requestBody",
					"type": "object",
					"info": "the payload to be sent with the request (optional)",
					"description": "the payload to be sent with the request (optional)",
					"schema": {
						"title": "requestBody",
						"type": "object"
					},
					"required": false
				},
				{
					"name": "addlHeaders",
					"type": "object",
					"info": "additional headers to be put on the call (optional)",
					"description": "additional headers to be put on the call (optional)",
					"schema": {
						"title": "addlHeaders",
						"type": "object"
					},
					"required": false
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/genericAdapterRequestNoBasePath"
			},
			"task": true
		},
		{
			"name": "getDevice",
			"summary": "Get the Appliance",
			"description": "Get the Appliance",
			"input": [
				{
					"name": "deviceName",
					"type": "string",
					"info": "An Appliance Device Name",
					"required": true,
					"schema": {
						"title": "deviceName",
						"type": "string"
					}
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/getDevice"
			},
			"task": false
		},
		{
			"name": "getDevicesFiltered",
			"summary": "Get Appliances that match the filter",
			"description": "Get Appliances that match the filter",
			"input": [
				{
					"name": "options",
					"type": "object",
					"info": "options - e.g. { 'start': 1, 'limit': 20, 'filter': { 'name': 'abc123' } }",
					"required": true,
					"schema": {
						"title": "options",
						"type": "object"
					}
				}
			],
			"output": {
				"name": "result",
				"type": "array",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "array"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/getDevicesFiltered"
			},
			"task": false
		},
		{
			"name": "isAlive",
			"summary": "Checks the status for the provided Appliance",
			"description": "Checks the status for the provided Appliance",
			"input": [
				{
					"name": "deviceName",
					"type": "string",
					"info": "An Appliance Device Name",
					"required": true,
					"schema": {
						"title": "deviceName",
						"type": "string"
					}
				}
			],
			"output": {
				"name": "result",
				"type": "boolean",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "boolean"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/isAlive"
			},
			"task": false
		},
		{
			"name": "getConfig",
			"summary": "Gets a config for the provided Appliance",
			"description": "Gets a config for the provided Appliance",
			"input": [
				{
					"name": "deviceName",
					"type": "string",
					"info": "An Appliance Device Name",
					"required": true,
					"schema": {
						"title": "deviceName",
						"type": "string"
					}
				},
				{
					"name": "format",
					"type": "string",
					"info": "The format to be returned - this is ignored as we always return json",
					"required": false,
					"schema": {
						"title": "format",
						"type": "string"
					}
				}
			],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/getConfig"
			},
			"task": false
		},
		{
			"name": "iapGetDeviceCount",
			"summary": "Gets a device count from the system",
			"description": "Gets a device count from the system",
			"input": [],
			"output": {
				"name": "result",
				"type": "object",
				"description": "A JSON Object containing status, code and the result",
				"schema": {
					"title": "result",
					"type": "object"
				}
			},
			"roles": [
				"admin"
			],
			"route": {
				"verb": "POST",
				"path": "/iapGetDeviceCount"
			},
			"task": false
		},
    {
      "name": "postCompileAndTest",
      "summary": "compileAndTest",
      "description": "Compile one or more Apex Classes, Triggers, and run tests.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postCompileAndTest"
      },
      "task": true
    },
    {
      "name": "compileApexClasses",
      "summary": "compileClasses",
      "description": "Compile one or more Apex Classes.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/compileApexClasses"
      },
      "task": true
    },
    {
      "name": "compileApexTriggers",
      "summary": "compileTriggers",
      "description": "Compile Apex Trigger code blocks.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/compileApexTriggers"
      },
      "task": true
    },
    {
      "name": "postExecuteAnonymous",
      "summary": "executeAnonymous",
      "description": "Execute an anonymous Apex code block",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postExecuteAnonymous"
      },
      "task": true
    },
    {
      "name": "runApexTests",
      "summary": "runTests",
      "description": "Execute test methods",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/runApexTests"
      },
      "task": true
    },
    {
      "name": "postWsdlToApex",
      "summary": "wsdlToApex",
      "description": "Generate Apex packages from WSDL for web service callouts",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postWsdlToApex"
      },
      "task": true
    }
  ],
  "views": []
}